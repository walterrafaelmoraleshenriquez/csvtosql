using System.Transactions;
using CsvHelper.Configuration;
using CsvToSql.Models;

namespace CsvToSql.Mappers
{
    public class TransactionMapper : ClassMap<TransactionModel>
    {
        public TransactionMapper()
        {
            Map(t => t.Status).Name("Status");
            Map(t => t.Date).Name("Date");
            Map(t => t.Description).Name("Description");
            Map(t => t.Debit).Name("Debit");
            Map(t => t.Credit).Name("Credit");
            Map(t => t.Category).Name("Category");
        }
    }
}