﻿using System.Globalization;
using System.Transactions;
using CsvHelper;
using CsvHelper.Configuration;
using CsvToSql.Context;
using CsvToSql.Mappers;
using CsvToSql.Models;
using EFCore.BulkExtensions;

Console.Write("Set file name: ");
var fileName = Console.ReadLine();

using var reader = new StreamReader(fileName!);
using (var csvReader = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)
{
    PrepareHeaderForMatch = args => args.Header.ToLower(),
    Delimiter = ","
}))
{
    csvReader.Context.RegisterClassMap<TransactionMapper>();
    var records = csvReader.GetRecords<TransactionModel>().ToList();

    using var context = new CsvToSqlDbContext();

    using var transaction = context.Database.BeginTransaction();
    context.BulkInsert(records);
    transaction.Commit();
}