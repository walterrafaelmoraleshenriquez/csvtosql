using CsvToSql.Models;
using Microsoft.EntityFrameworkCore;

namespace CsvToSql.Context
{
    public class CsvToSqlDbContext : DbContext
    {
        public DbSet<TransactionModel> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) =>
            options.UseSqlServer("Server=localhost;Database=TransactionsDb;Trusted_Connection=true;");

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<TransactionModel>()
                .HasKey(e => e.Id);
        }
    }
}