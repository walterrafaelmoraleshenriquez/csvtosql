using CsvHelper.Configuration.Attributes;

namespace CsvToSql.Models 
{
    public class TransactionModel
    {
        public int Id { get; set; }

        public string? Status { get; set; }

        public DateTime Date { get; set; }

        public string? Description { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public string? Category { get; set; }
    }
}